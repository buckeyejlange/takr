﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Takr.Core;
using Takr.UI;
using Xunit;

namespace Takr.Test.UI
{
	public class AddMessageViewModelTest
	{
		[Trait("Category", "Unit")]
		[Trait("Class", "AddMessageViewModel")]
		public class AddMessage
		{
			[Fact]
			public async Task NullRepository()
			{
				var target = new AddMessageViewModel(null)
				{
					MessageText = "Test"
				};

				await target.AddMessage.ExecuteAsync("");
			}

			[Fact]
			public async Task NotifiesSubscribers()
			{
				Mock<IMessageDataRepository> mockRepo = new Mock<IMessageDataRepository>();
				mockRepo.Setup(r => r.WriteAsync(It.Is<Message>(m => m.Text == "Test"))).ReturnsAsync(1).Verifiable();

				var target = new AddMessageViewModel(mockRepo.Object)
				{
					MessageText = "Test"
				};
				
				await target.AddMessage.ExecuteAsync("");
				
				mockRepo.Verify();
			}

			[Fact]
			public async Task MultipleCalls()
			{
				Mock<IMessageDataRepository> mockRepo = new Mock<IMessageDataRepository>();
				mockRepo.Setup(r => r.WriteAsync(It.Is<Message>(m => m.Text == "Test"))).ReturnsAsync(1).Verifiable();
				mockRepo.Setup(r => r.WriteAsync(It.Is<Message>(m => m.Text == String.Empty))).ReturnsAsync(1).Verifiable();

				var target = new AddMessageViewModel(mockRepo.Object)
				{
					MessageText = "Test"
				};

				await Task.WhenAll(
					target.AddMessage.ExecuteAsync(""), 
					target.AddMessage.ExecuteAsync(""), 
					target.AddMessage.ExecuteAsync(""), 
					target.AddMessage.ExecuteAsync(""));
				
				mockRepo.Verify(r => r.WriteAsync(It.Is<Message>(m => m.Text == "Test")), Times.Once);
				mockRepo.Verify(r => r.WriteAsync(It.Is<Message>(m => m.Text == String.Empty)), Times.Exactly(3));
			}

			[Fact]
			public async Task ClearsMessageText()
			{
				Mock<IMessageDataRepository> mockRepo = new Mock<IMessageDataRepository>();
				mockRepo.Setup(r => r.WriteAsync(It.IsAny<Message>())).ReturnsAsync(1);

				var target = new AddMessageViewModel(mockRepo.Object)
				{
					MessageText = "Test"
				};

				await target.AddMessage.ExecuteAsync("");

				Assert.Equal(String.Empty, target.MessageText);
			}
		}
	}
}
