﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Takr.Core;

namespace Takr.Data
{
	/// <summary>
	/// An implementation of <see cref="IMessageDataRepository"/> that stores data in a sqlite databse.
	/// </summary>
    public class SQLiteMessageDataRepository : IMessageDataRepository
    {
        private string _filePath;
		
		private readonly string CreateTableCommand = $@"
CREATE TABLE {nameof(Message)}s
(
    {nameof(Message.Timestamp)} DATETIME,
    {nameof(Message.Text)} TEXT
)
";

		private readonly string InsertCommand = $"INSERT INTO Messages ({nameof(Message.Timestamp)}, {nameof(Message.Text)}) VALUES (@{nameof(Message.Timestamp)}, @{nameof(Message.Text)})";
		private readonly string NewerThanTimestampWhereClause = $"{nameof(Message.Timestamp)} > @{nameof(Message.Timestamp)}";

		/// <summary>
		/// Initializes a new instance of the <see cref="SQLiteMessageDataRepository"/> class.
		/// </summary>
		/// <param name="filePath">The path to write the sqlite db to.</param>
		public SQLiteMessageDataRepository(string filePath)
		{
			_filePath = filePath;
			CreateIfNotExists();
		}

		// TODO: Inherit
        public async Task<IEnumerable<Message>> GetAllAsync()
        {
            using (var conn = GetConnection())
            {
                return await conn.QueryAsync<Message>(CreateSelectCommand());
            }
        }

		// TODO: Inherit
		public async Task<int> WriteAsync(Message item)
        {
            using (var conn = GetConnection())
            {
                return await conn.ExecuteAsync(InsertCommand, item);
            }
        }

		// TODO: Inherit
		public async Task<IEnumerable<Message>> GetWithinTimespanAsync(TimeSpan timespan)
        {
            using (var conn = GetConnection())
            {
                string selectQuery = CreateSelectCommand(NewerThanTimestampWhereClause);
                return await conn.QueryAsync<Message>(selectQuery, new Message { Timestamp = ToDateTime(timespan) });
            }
        }

		private void CreateIfNotExists()
		{
			if (!File.Exists(_filePath))
			{
				SQLiteConnection.CreateFile(_filePath);

				using (var conn = GetConnection())
				{
					conn.Execute(CreateTableCommand);
				}
			}
		}

		private SQLiteConnection GetConnection()
        {
            return new SQLiteConnection(String.Format("Data Source={0};", _filePath));
        }
		
        private string CreateSelectCommand(params string[] whereClauses)
        {
            string select = $"SELECT * FROM {nameof(Message)}s";

            if (whereClauses.Length > 0)
            {
                select += " WHERE ";
                select += String.Join(" AND ", whereClauses);
            }

            return select;
        }

        private DateTime ToDateTime(TimeSpan span)
        {
			if (span < TimeSpan.Zero)
				throw new ArgumentException("Span must be greater than 0.", "span");

			DateTime timeToQuery;

			try
			{
				timeToQuery = DateTime.UtcNow.Subtract(span);
			}
			catch (ArgumentOutOfRangeException)
			{
				// Does it make sense to throw? This really shouldn't come up.
				timeToQuery = DateTime.MinValue;
			}

			return timeToQuery;
        }
    }
}
