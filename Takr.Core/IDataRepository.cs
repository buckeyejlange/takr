﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Takr.Core
{
	/// <summary>
	/// Provides a mechanism for storing and retrieving objects of type {T}.
	/// </summary>
	/// <typeparam name="T">The type of objects to store/retrieve.</typeparam>
    public interface IDataRepository<T> where T : class
    {
		/// <summary>
		/// Asynchronously writes the item to the repository.
		/// </summary>
		/// <param name="item">The item to write.</param>
		/// <returns>A task that indicates when the operation will complete.</returns>
        Task<int> WriteAsync(T item);
        
		/// <summary>
		/// Asynchronously gets all items in the repository.
		/// </summary>
		/// <returns>A task that contains the result of the operation.</returns>
        Task<IEnumerable<T>> GetAllAsync();
    }
}
