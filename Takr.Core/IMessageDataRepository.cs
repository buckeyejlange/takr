﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Takr.Core
{
	/// <summary>
	/// Provides a mechanism for storing and retrieving messages.
	/// </summary>
    public interface IMessageDataRepository : IDataRepository<Message>
    {
		/// <summary>
		/// Asynchronously gets all messages with a Timestamp within the provided timespan.
		/// </summary>
		/// <param name="timespan">The timespan to search for them.</param>
		/// <returns>A task that contains the result of the operation.</returns>
		Task<IEnumerable<Message>> GetWithinTimespanAsync(TimeSpan timespan);
    }
}
