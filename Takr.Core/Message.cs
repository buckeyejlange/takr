﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Takr.Core
{
    public class Message
    {
        public string Text { get; set; }

        public DateTime Timestamp { get; set; }
    }
}
