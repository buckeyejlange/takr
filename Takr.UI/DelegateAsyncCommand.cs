﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Takr.UI
{
	/// <summary>
	/// <para>
	/// Class that implements <see cref="ICommand"/> using an asynchronous delegate.
	/// </para>
	/// <para>
	/// Inspired by: https://msdn.microsoft.com/en-us/magazine/dn630647.aspx
	/// </para>
	/// </summary>
	/// <typeparam name="T">The type of object passed into delegates.</typeparam>
	/// <remarks>
	/// Still a WIP. Need to address error handling and the ability to cancel. For now this is a start.
	/// </remarks>
	public class DelegateAsyncCommand<T> : IAsyncCommand where T : class
	{
		private readonly Predicate<T> _canExecute;
		private readonly Func<T, Task> _execute;

		/// <summary>
		/// Initializes a new instance of the <see cref="DelegateAsyncCommand{T}"/> class.
		/// </summary>
		/// <param name="execute">The action to perform on "Execute".</param>
		public DelegateAsyncCommand(Func<T, Task> execute)
			: this(execute, null)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DelegateAsyncCommand{T}"/> class.
		/// </summary>
		/// <param name="execute">The action to perform on "Execute".</param>
		/// <param name="canExecute">The action to determine whether the command can be executed.</param>
		public DelegateAsyncCommand(Func<T, Task> execute, Predicate<T> canExecute)
		{
			_execute = execute;
			_canExecute = canExecute;
		}

		// TODO: Inherit
		public bool CanExecute(object parameter)
		{
			if (_canExecute == null)
			{
				return true;
			}

			return _canExecute((T)parameter);
		}

		// TODO: Inherit
		public async void Execute(object parameter)
		{
			await ExecuteAsync(parameter);
		}

		// TODO: Inherit
		public async Task ExecuteAsync(object parameter)
		{
			await _execute((T)parameter);
		}

		// TODO: Inherit
		public event EventHandler CanExecuteChanged;

		/// <summary>
		/// Method that allows for manually triggering <see cref="CanExecuteChanged"/> .
		/// </summary>
		public void RaiseCanExecuteChanged()
		{
			CanExecuteChanged?.Invoke(this, EventArgs.Empty);
		}
	}
}
