﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace Takr.UI
{
	public interface IAsyncCommand : ICommand
	{
		Task ExecuteAsync(object parameter);
	}
}
