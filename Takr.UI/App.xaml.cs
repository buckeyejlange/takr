﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using StructureMap;
using Takr.UI.Registries;

namespace Takr.UI
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		// TODO: Inherit
		protected override void OnStartup(StartupEventArgs e)
		{
			var registry = new Registry();
			registry.IncludeRegistry<DataRegistry>();
			registry.IncludeRegistry<UIRegistry>();

			var container = new Container(registry);
			MainWindow = container.GetInstance<MainWindow>();

			MainWindow.Show();
		}
	}
}
