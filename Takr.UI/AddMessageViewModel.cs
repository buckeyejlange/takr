﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Takr.Core;

namespace Takr.UI
{
	/// <summary>
	/// Provides a mechanism for a view model for pages that require adding messages.
	/// </summary>
	public interface IAddMessageViewModel
	{
		/// <summary>
		/// Gets a command to add a new message.
		/// </summary>
		IAsyncCommand AddMessage { get; }

		/// <summary>
		/// Gets or sets the text of the new message.
		/// </summary>
		string MessageText { get; set; }
	}

	/// <summary>
	/// View model for window that adds messages.
	/// </summary>
	public class AddMessageViewModel : IAddMessageViewModel
	{
		private string _text;
		private DelegateAsyncCommand<string> _addMessageCommand;
		private IMessageDataRepository _repo;

		// TODO: Inherit
		public IAsyncCommand AddMessage { get { return _addMessageCommand; } }

		// TODO: Inherit
		public string MessageText
		{
			get { return _text; }
			set
			{
				_text = value;
				_addMessageCommand.RaiseCanExecuteChanged();
			}
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="AddMessageViewModel"/> class.
		/// </summary>
		/// <param name="repo">An implementation of <see cref="IMessageDataRepository"/>.</param>
		/// <remarks>
		/// If we ever need to, it may be worth abstracting out the repository into a proper "model". Since it's really small
		/// right now I don't see the need for that. It's just added complexitiy IMO.
		/// </remarks>
		public AddMessageViewModel(IMessageDataRepository repo)
		{
			_repo = repo;
			_addMessageCommand = new DelegateAsyncCommand<string>(
				m => this.AddNewMessage(),
				m => { return !String.IsNullOrEmpty(MessageText); });
		}

		// TODO: It's kosher, but apparently theres more to it.
		// Need to finish reading this https://msdn.microsoft.com/en-us/magazine/dn630647.aspx
		private async Task AddNewMessage()
		{
			Message message = new Message
			{
				Text = MessageText,
				Timestamp = DateTime.UtcNow
			};

			if (_repo != null)
			{
				await _repo.WriteAsync(message);
			}

			MessageText = String.Empty;
		}
	}
}
