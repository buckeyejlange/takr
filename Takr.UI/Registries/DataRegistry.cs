﻿using StructureMap;
using Takr.Core;
using Takr.Data;

namespace Takr.UI.Registries
{
	/// <summary>
	/// Registry for the data/backend components.
	/// </summary>
	public class DataRegistry : Registry
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="DataRegistry"/> class.
		/// </summary>
		public DataRegistry()
		{
			// The db name will eventually go into a config file or something.
			For<IMessageDataRepository>().Use<SQLiteMessageDataRepository>().Ctor<string>().Is("db.sqlite");
		}
	}
}
