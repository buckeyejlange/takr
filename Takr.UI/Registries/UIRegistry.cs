﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StructureMap;

namespace Takr.UI.Registries
{
	/// <summary>
	/// Registry for the UI components.
	/// </summary>
	public class UIRegistry : Registry
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="UIRegistry"/> class.
		/// </summary>
		public UIRegistry()
		{
			Scan(x =>
			{
				x.TheCallingAssembly();
				x.WithDefaultConventions();
			});
		}
	}
}
