﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Takr.UI
{
	/// <summary>
	/// <para>
	/// Class that implements <see cref="ICommand"/> using the provided delegate.
	/// </para>
	/// <para>
	/// Taken from: https://social.technet.microsoft.com/wiki/contents/articles/18199.event-handling-in-an-mvvm-wpf-application.aspx
	/// </para>
	/// </summary>
	/// <typeparam name="T">The type of object passed into delegates.</typeparam>
	public class DelegateCommand<T> : ICommand where T : class
	{
		private readonly Predicate<T> _canExecute;
		private readonly Action<T> _execute;

		/// <summary>
		/// Initializes a new instance of the <see cref="DelegateCommand{T}"/> class.
		/// </summary>
		/// <param name="execute">The action to perform on "Execute".</param>
		public DelegateCommand(Action<T> execute)
			: this(execute, null)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DelegateCommand{T}"/> class.
		/// </summary>
		/// <param name="execute">The action to perform on "Execute".</param>
		/// <param name="canExecute">The action to determine whether the command can be executed.</param>
		public DelegateCommand(Action<T> execute, Predicate<T> canExecute)
		{
			_execute = execute;
			_canExecute = canExecute;
		}

		// TODO: Inherit
		public bool CanExecute(object parameter)
		{
			if (_canExecute == null)
			{
				return true;
			}

			return _canExecute((T)parameter);
		}

		// TODO: Inherit
		public void Execute(object parameter)
		{
			_execute((T)parameter);
		}

		// TODO: Inherit
		public event EventHandler CanExecuteChanged;

		/// <summary>
		/// Method that allows for manually triggering <see cref="CanExecuteChanged"/> .
		/// </summary>
		public void RaiseCanExecuteChanged()
		{
			CanExecuteChanged?.Invoke(this, EventArgs.Empty);
		}
	}
}
