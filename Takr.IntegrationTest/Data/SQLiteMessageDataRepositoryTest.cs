﻿using System;
using System.IO;
using System.Threading.Tasks;
using Takr.Core;
using Takr.Data;
using Dapper;
using Xunit;
using System.Data.SQLite;
using System.Linq;

namespace Takr.IntegrationTest.Data
{
    [Collection("Integration")]
    [Trait("Category", "Integration")]
    public class SQLiteMessageDataRepositoryTest : IDisposable
    {
        private string _dbPath = Environment.CurrentDirectory + @"\db.sqlite";

        public void Dispose()
        {
            try
            {
                File.Delete(_dbPath);
            }
            catch (Exception)
            {
                // Something could have went wrong during test.
                // For an integration test I'm not that worried.
            }
        }

        [Fact]
        public void Constructor_CreatesNewDb()
        {
            var target = SetupTarget();

            Assert.True(File.Exists(_dbPath));
        }

		[Fact]
		public async Task Write_WritesItem()
		{
			var target = SetupTarget();

			var numberOfRecords = target.WriteAsync(new Message
			{
				Timestamp = DateTime.Parse("2017-01-01"),
				Text = "Test"
			});

			Assert.Equal(1, await numberOfRecords);

			using (var connection = new SQLiteConnection("Data Source=" + _dbPath))
			{
				var items = await connection.QueryAsync<Message>("SELECT * FROM Messages");

				Assert.Equal(1, items.Count());
				Assert.Equal("Test", items.First().Text);
				Assert.Equal(DateTime.Parse("2017-01-01"), items.First().Timestamp);
			}
		}

		[Fact]
		public async Task Write_HandlesBadCharacters()
		{
			var target = SetupTarget();

			var numberOfRecords = target.WriteAsync(new Message
			{
				Timestamp = DateTime.Parse("2017-01-01"),
				Text = "T'es't#$%^&*(())!@@##$[]{};:'\"?><"
			});

			Assert.Equal(1, await numberOfRecords);

			using (var connection = new SQLiteConnection("Data Source=" + _dbPath))
			{
				var items = await connection.QueryAsync<Message>("SELECT * FROM Messages");

				Assert.Equal(1, items.Count());
				Assert.Equal("T'es't#$%^&*(())!@@##$[]{};:'\"?><", items.First().Text);
				Assert.Equal(DateTime.Parse("2017-01-01"), items.First().Timestamp);
			}
		}

		[Fact]
		public async Task GetAllAsync_RetrievesAll()
		{
			var target = SetupTarget();
			
			var numberOfRecords = target.WriteAsync(new Message
			{
				Timestamp = DateTime.Parse("2017-01-01"),
				Text = "Test"
			});
			
			var numberOfRecords2 = target.WriteAsync(new Message
			{
				Timestamp = DateTime.Parse("2017-01-02"),
				Text = "Test2"
			});

			await Task.WhenAll(numberOfRecords, numberOfRecords2);

			var items = await target.GetAllAsync();

			Assert.Equal(2, items.Count());
			Assert.Equal("Test", items.First().Text);
			Assert.Equal(DateTime.Parse("2017-01-01"), items.First().Timestamp);
			Assert.Equal("Test2", items.Last().Text);
			Assert.Equal(DateTime.Parse("2017-01-02"), items.Last().Timestamp);
		}

		[Fact]
		public async Task GetWithinTimespan_CorrectlyLimitsRecords()
		{
			var target = SetupTarget();
			
			var numberOfRecords = target.WriteAsync(new Message
			{
				Timestamp = DateTime.UtcNow,
				Text = "Test"
			});

			var numberOfRecords2 = target.WriteAsync(new Message
			{
				Timestamp = DateTime.Parse("2017-01-02"),
				Text = "Test2"
			});

			await Task.WhenAll(numberOfRecords, numberOfRecords2);

			var items = await target.GetWithinTimespanAsync(TimeSpan.FromDays(1));

			Assert.Equal(1, items.Count());
			Assert.Equal("Test", items.First().Text);
		}

		[Fact]
		public async Task GetWithinTimespan_TimespanZero()
		{
			var target = SetupTarget();
			
			var numberOfRecords = target.WriteAsync(new Message
			{
				Timestamp = DateTime.UtcNow.AddSeconds(-1),
				Text = "Test"
			});

			var numberOfRecords2 = target.WriteAsync(new Message
			{
				Timestamp = DateTime.Parse("2017-01-02"),
				Text = "Test2"
			});

			await Task.WhenAll(numberOfRecords, numberOfRecords2);

			var items = await target.GetWithinTimespanAsync(TimeSpan.Zero);

			Assert.Equal(0, items.Count());
		}

		[Fact]
		public async Task GetWithinTimespanAsync_MaxValue()
		{
			var target = SetupTarget();
			
			var numberOfRecords = target.WriteAsync(new Message
			{
				Timestamp = DateTime.Parse("2017-01-01"),
				Text = "Test"
			});

			var numberOfRecords2 = target.WriteAsync(new Message
			{
				Timestamp = DateTime.Parse("2017-01-02"),
				Text = "Test2"
			});

			await Task.WhenAll(numberOfRecords, numberOfRecords2);

			var items = await target.GetWithinTimespanAsync(TimeSpan.MaxValue);

			Assert.Equal(2, items.Count());
			Assert.Equal("Test", items.First().Text);
			Assert.Equal(DateTime.Parse("2017-01-01"), items.First().Timestamp);
			Assert.Equal("Test2", items.Last().Text);
			Assert.Equal(DateTime.Parse("2017-01-02"), items.Last().Timestamp);
		}

		private SQLiteMessageDataRepository SetupTarget()
        {
            return new SQLiteMessageDataRepository(_dbPath);
        }
    }
}
