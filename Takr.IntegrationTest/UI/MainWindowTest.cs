﻿using System;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Threading;
using Dapper;
using Takr.Core;
using Takr.UI.Resources;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.WindowItems;
using Xunit;
using static TestStack.White.WindowsAPI.KeyboardInput;

namespace Takr.IntegrationTest.DesktopUI
{
	[Collection("Integration")]
	[Trait("Category", "Integration")]
	public class MainWindowTest : IDisposable
	{
		private readonly string _applicationPath = Environment.CurrentDirectory + @"\Takr.UI.exe";
		private readonly string _defaultDbPath = Environment.CurrentDirectory + @"\db.sqlite";

		public void Dispose()
		{
			try
			{
				File.Delete(_defaultDbPath);
			}
			catch (Exception ex)
			{
				// Something could have went wrong during test.
				// For an integration test I'm not that worried.
			}
		}

		[Fact]
		public void AddButton_AddsMessageToDb()
		{
			Application app = Application.Launch(_applicationPath);

			try
			{
				Window window = app.GetWindow(Strings.Takr);

				TextBox textBox = window.Get<TextBox>("MessageBox");
				textBox.BulkText = "This is a text message";

				Button addButton = window.Get<Button>("AddButton");
				addButton.Click();

				Thread.Sleep(1000);
				using (var conn = new SQLiteConnection("DataSource=" + _defaultDbPath))
				{
					var Messages = conn.Query<Message>("SELECT * FROM Messages ORDER BY Timestamp DESC LIMIT 1");

					Assert.Equal("This is a text message", Messages.First().Text);
				}
			}
			finally
			{
				app.Close();
			}
		}

		[Fact]
		public void HittingEnter_AddsMessageToDb()
		{
			Application app = Application.Launch(_applicationPath);

			try
			{
				Window window = app.GetWindow(Strings.Takr);

				TextBox textBox = window.Get<TextBox>("MessageBox");
				textBox.BulkText = "This is a text message";
				textBox.KeyIn(SpecialKeys.RETURN);

				Thread.Sleep(1000);

				using (var conn = new SQLiteConnection("DataSource=" + _defaultDbPath))
				{
					var Messages = conn.Query<Message>("SELECT * FROM Messages ORDER BY Timestamp DESC LIMIT 1");

					Assert.Equal("This is a text message", Messages.First().Text);
				}
			}
			finally
			{
				app.Close();
			}
		}
	}
}
